#!/usr/bin/env python3

from datetime import datetime
import os
import warnings
import subprocess
from numpy import get_include as _numpy_get_include
from setuptools import setup, Extension
from future.utils import raise_from

# 0.0.0-dev.* version identifiers for development only
__version__ = "0.0.0.dev" + datetime.now().strftime("%Y%m%d")

def main():
    setup(
        name="ipyopt",
        version=__version__,
        description="An IPOpt connector for Python",
        long_description=open("README.md").read(),
        long_description_content_type="text/markdown",
        author="Gerhard",
        url="https://github.com/g-braeunlich/ipyopt",
        ext_modules=[
            Extension("ipyopt",
                      sources=["src/callback.c",
                               "src/ipyopt_module.c"],
                      **get_compiler_flags())],
        install_requires=["numpy"],
    )

def get_compiler_flags():
    """Tries to find all needed compiler flags needed to compile the extension
    """
    compiler_flags = {"include_dirs": [_numpy_get_include()]}
    try:
        return pkg_config("ipopt", **compiler_flags)
    except RuntimeError as e:
        if 'CFLAGS' not in os.environ:
            warnings.warn(
                "pkg-config not installed or malformed pc file.\n"
                "Message from pkg-config:\n{}\n\n"
                "You have to provide setup.py with the include and library "
                "directories of IPOpt. Example:\n"
                "CFLAGS='-I/usr/include/coin/ -l/usr/lib64 "
                "-lipopt -lmumps_common -ldmumps -lzmumps -lsmumps "
                "-lcmumps -llapack -lblas -lblas -lblas "
                "-lm  -ldl' ./setup.py build".format(e.args[0]))
        return compiler_flags
def pkg_config(*packages, **kwargs):
    """Calls pkg-config returning a dict containing all arguments
    for Extension() needed to compile the extension
    """
    flag_map = {b'-I': 'include_dirs',
                b'-L': 'library_dirs',
                b'-l': 'libraries',
                b'-D': 'define_macros'}
    try:
        res = run(
            ("pkg-config", "--libs", "--cflags")
            + packages, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
    except subprocess.CalledProcessError as e:
        raise_from(RuntimeError(e.stderr.decode()),e)
    for token in res.stdout.split():
        kwargs.setdefault(flag_map.get(token[:2]), []).append(
            token[2:].decode())
    define_macros = kwargs.get('define_macros')
    if define_macros:
        kwargs['define_macros'] = [tuple(d.split()) for d in define_macros]
    undefined_flags = kwargs.pop(None, None)
    if undefined_flags:
        warnings.warn(
            "Ignoring flags {} from pkg-config".format(
                ", ".join(undefined_flags)))
    return kwargs

def run(*popenargs, **kwargs):
    input = kwargs.pop("input", None)
    check = kwargs.pop("handle", False)

    if input is not None:
        if 'stdin' in kwargs:
            raise ValueError('stdin and input arguments may not both be used.')
        kwargs['stdin'] = subprocess.PIPE

    process = subprocess.Popen(*popenargs, **kwargs)
    try:
        stdout, stderr = process.communicate(input)
    except:
        process.kill()
        process.wait()
        raise
    retcode = process.poll()
    if check and retcode:
        raise subprocess.CalledProcessError(
            retcode, process.args, output=stdout, stderr=stderr)
    return retcode, stdout, stderr

main()
